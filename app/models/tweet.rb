class Tweet < ApplicationRecord
  validates :text,
            presence: true,
            length: {
              maximum: 140,
              too_long: '%<count>s characters is the maximum allowed'
            }
end
