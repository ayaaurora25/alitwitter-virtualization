require 'rails_helper'

describe 'Tweet Model', type: :model do
  describe 'validations tests' do
    context 'given empty text params' do
      it 'return false' do
        tweet = Tweet.new
        expect(tweet.valid?).to eq(false)
      end
    end

    context 'given text params exceeded 140 characters' do
      it 'return false' do
        text = 'hello world'
        20.times do
          text << 'hello world'
        end
        tweet = Tweet.new(text: text)
        expect(tweet.valid?).to eq(false)
      end
    end

    context 'given valid text params' do
      it 'should be able to save tweet' do
        tweet = Tweet.new(text: 'Test tweet')
        expect(tweet.save).to eq(true)
      end
    end
  end
end
