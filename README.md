# AliTwitter App

## Description
This application only used for a single user. This application allows the user to post a tweet, see all his tweets, and delete a tweet. 

## Environment Setup
There are several tools that you need to install in your machine before you can use this application. There are:
- [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
- [Vagrant](https://www.vagrantup.com/intro/index.html)
- [Ansible](https://docs.ansible.com/ansible/latest/index.html)

These tools are needed in order to create VM and do the provisioning.

## VM Preparation
In order to do the CI/CD virtualization works, you need to create VMs using the provided `Vagrantfile` within this project root directory. There will be 5 VMs created by executing the `Vagrantfile`, there are:
- `db`: a database server VM
- `loadbalancer`: a loadbalancer VM
- `app1` and `app2`: two webservers VMs
- `runner`: a runner (gitlab-runner) VM

For these project, the provisioning is done to all created VMs except the runner VM. As for the runner VM, you can set it up by following this [instruction](##runner-setup).

Before preparing the VMs, first thing first, you need to make the VMs are running by typing this command in this project root directory terminal
```bash
vagrant up
```

If you prefer to run the VM one by one, you should type
```bash
vagrant up <vm-name>
```

Example to run the database server:
```bash
vagrant up db
```

To get the VMs prepared, you need to execute the following ansible-playbook command in this project root directory terminal
```bash
ansible-playbook -i ansible/configuration/inventory/hosts.yml ansible/configuration/site.yml
```

If you prefer to set the VM one at a time, you can change the `ansible/configuration/site.yml` playbook with one of the following playbooks:
- `ansible/configuration/configure_dbserver.yml`
- `ansible/configuration/configure_loadbalancer.yml`
- `ansible/configuration/configure_webservers.yml`

## Runner Setup
Before you can see the CI/CD running properly, you need to setup the runner first. The runner setups actually pretty the same as the webservers setup because it has to do the code base testing. Apart from that, you also need to install the gitlab-runner by following its installation guide in [here](https://docs.gitlab.com/runner/install/linux-manually.html).

Therefore, tools that you need to install within your runner VM are:
- [Ruby 2.6.5](https://www.ruby-lang.org/en/downloads/)
- [Bundler](https://bundler.io/)
- [NodeJS](https://nodejs.org/en/download/package-manager/)
- [Yarn](https://classic.yarnpkg.com/en/docs/install/#debian-stable)
- [Gitlab-runner](https://docs.gitlab.com/runner/install/linux-manually.html)
- [Ansible](https://docs.ansible.com/ansible/latest/index.html)
- zip and unzip package in ubuntu

Once you have these tools installed, you need to create a repo in [gitlab](https://gitlab.com/) to clearly see the running pipeline. After creating the repo, you need to setup the gitlab CI/CD by following this [guide](https://docs.gitlab.com/ee/ci/quick_start/) provided by gitlab.

To recap the gitlab-runner setup, first thing you have to after gitlab-runner installation is registering your repo into your runner. But before that, you need to have an access to your runner by typing this command in this project root directory
```bash
vagrant ssh runner
```
After that, you need to change user to `gitlab-runner` by typing this command in runner VM
```bash
sudo su gitlab-runner
```
Once you are a `gitlab-runner` user, you are able to register your repo by typing
```bash
gitlab-runner register
```
then, there will be several questions you need to answers, the example is shown below:
```bash
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
https://gitlab.com
Please enter the gitlab-ci token for this runner:
<your repo token>
Please enter the gitlab-ci description for this runner:
[ubuntu-bionic]: ubuntu-runner
Please enter the gitlab-ci tags for this runner (comma separated):
shell, ubuntu
Please enter the executor: ssh, docker+machine, docker-ssh+machine, kubernetes, docker, parallels, virtualbox, docker-ssh, shell:
shell
```
For the executor type, please type `shell` and for the tags please type `shell, ubuntu`.

In order to know your repo token, you need to go to your repo **Settings -> CI/CD -> Runners**. Within that configuration, you need to disable the shared runners. To set the used secret variables in this project, like SECRET_KEY_BASE and RAILS_DB_PASSWORD, you should set it up in **Settings -> CI/CD -> Variables**. 

Once the repo registration is done, you need to start the gitlab-runner by typing
```bash
gitlab-runner start
```

and to verify that your gitlab-runner is ready, you can type this command
```bash
gitlab-runner verify
```

Now, your gitlab-runner will be triggered everytime you push a change to your repo.

## Runner Provisioning Preparation

### Private Key Webserver Preparation
After the gitlab-runner setup, in order to make the provisioning works within your runner, you need to make sure that the _private keys_ of each webserver exists at `~/.ssh/private_keys/`. One way to achieve that goal, we can copy the private key by running the command `vagrant scp` for each private key to the runner VM. To ease this step, you need to copy each private key of the webservers to `ansible/deployment/private_keys` and rename it into `app1_private_key` for app1's private key and `app2_private_key` for app2's private key.

To copy the private keys, in the project root directory terminal you need to type
```bash
vagrant scp ansible/deployment/private_keys runner:~/.ssh/
```

After that, you need to access the runner by typing
```bash
vagrant ssh runner
```
and copy or move the private keys to gitlab-runner user
```bash
sudo su
cd /home
cp -r vagrant/.ssh/private_keys gitlab-runner/.ssh
exit
```

### Rails Environment Preparation
Any change to SECRET_KEY_BASE is possible by changing its value in your gitlab CI/CD settings variables and in `ansible/deployment/group_vars/all.yml`.

Any change to RAILS environment and database configuration is also possible. You can do that in `.gitlab-ci.yml` and `ansible/configuration/group_vars/all.yml`. Both of these files need to be changed with your desired configuration.

## Run the CI/CD
In order to see that your pipeline is working, you need to `push` a change to your gitlab repo. You can access the example of the pipeline result in [here](https://gitlab.com/ayaaurora25/alitwitter-virtualization/pipelines).

Once you pass all the stages of the pipeline (test, build, and deploy), you can access the application by typing `https://192.168.50.11:3000/tweets` in your browser if your loadbalancer is running. You can also access the application by typing `https://192.168.50.12:3000/tweets` and `https://192.168.50.13:3000/tweets` if you prefer to access it directly to the webserver.

To check the status of your loadbalancer, you can see that by typing `https://192.168.50.11:1936` in your browser.
